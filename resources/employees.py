from flask_restplus import Namespace, Resource, reqparse,fields,Model
from models.employee_model import Employee
import json
ns = Namespace('employees',description="Entire list of employees")

emp_model = Model('emp_model',{
            "id" : fields.Integer,
            "name": fields.String,
            "email": fields.String,
            "designation": fields.String,
            "gender": fields.String,
            "super_id": fields.Integer
})

@ns.route('/list')
class Employees(Resource):
    @ns.marshal_list_with(emp_model)
    def get(self):
        employees = Employee.query.all()
        response = { "emp_list": []}
        for employee in employees:
            response["emp_list"].append(employee.to_dict())
        return response

@ns.route("/<string:email>")
class EmployeeMail(Resource):
    @ns.marshal_with(emp_model)
    def get(self,email):
        employee = Employee.query.filter_by(email=email).first()
        if employee:
            return employee.to_dict()
        else:
            return employee

@ns.route("/<int:id>")
class EmployeeId(Resource):
    @ns.marshal_with(emp_model)
    def get(self,id):
        employee = Employee.query.filter_by(id=id).first()
        if employee:
            return employee.to_dict()
        else:
            return employee