from flask_restplus import Namespace, Resource, fields, reqparse
from flask_mail import Message
from utils.mail import mail
from utils.db import db
from models.employee_model import Employee
from werkzeug.datastructures import FileStorage
from werkzeug.security import generate_password_hash
import csv
import uuid
from flask_jwt import jwt_required, current_identity

ns = Namespace('csv', description='Upload csv of employees')

parser = reqparse.RequestParser()
parser.add_argument('csv',type= FileStorage, location='files', required=True)

@ns.route('/onboarding')
class Onboarding(Resource):
    @ns.expect(parser)
    @jwt_required()
    def post(self):
        print("In post")
        args = parser.parse_args()
        # print(args[csv])
        f = args['csv']
        print("filename",f.filename)
        f.save(f.filename)
        print(f.filename)
        csv_file = open(f.filename, 'r')
        csv_reader = csv.DictReader(csv_file)
        with mail.connect() as conn:
            for row in csv_reader:
                password = uuid.uuid4().hex[0:8]
                if ( Employee.query.filter_by(email=row["email"]).first() is None 
                        and Employee.query.filter_by(mobile_no=row["mobile_no"]).first() is None) :
                    employee = Employee(
                        name = row['name'],
                        email = row['email'],
                        designation = row['designation'],
                        age = row['age'],
                        mobile_no = row['mobile_no'],
                        gender = row['gender'],
                        super_id = row['super_id'],
                        password =  generate_password_hash(password)
                        )
                    db.session.add(employee)
                    message = f'''
                                Hi {employee.name}, This is a test body
                                Here are your login credentials for HRMS!
                                email:-  {employee.email}
                                password:-  {password}
                                
                        '''
                    msg = Message(recipients=[employee.email],body=message ,subject="LOGIN CREDENTIALS")
                    conn.send(msg)
        db.session.commit()    
        return {"response": "success"},200