from flask import request
from flask_restplus import Api
from .csv import ns as csv
from .employees import ns as employees

api = Api(
    title='Employee Onboarding',
    version='1.0',
    description='Upload a csv of employees and authentication',
    # All API metadatas
)

api.add_namespace(csv)
api.add_namespace(employees)



