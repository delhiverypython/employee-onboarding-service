from utils.db import db

class Employee(db.Model):
    id = db.Column(db.Integer, primary_key = True, unique = True, nullable = False)
    name = db.Column(db.Text, nullable = False)
    email = db.Column(db.Text, unique = True, nullable = False)
    password = db.Column(db.Text, nullable = False)
    designation = db.Column(db.Text, nullable = False)
    age = db.Column(db.Integer, nullable = False)
    mobile_no = db.Column(db.Text, unique = True, nullable = False)
    gender = db.Column(db.Text, nullable = False)
    super_id = db.Column(db.Integer, db.ForeignKey('employee.id'),nullable = False)

    def to_dict(self):
        return {
            "id" : self.id,
            "name": self.name,
            "email": self.email,
            "password": self.password,
            "designation": self.designation,
            "age":self.age,
            "gender": self.gender,
            "mobile_no":self.mobile_no,
            "super_id": self.super_id
        }